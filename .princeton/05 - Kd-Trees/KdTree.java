public class KdTree {
    private Node root;
    private int N;

    private static class Node {
        private Point2D p;   // the point
        private RectHV rect; // the axis-aligned rectangle corresponding to this node
        private Node lb;     // the left/bottom subtree
        private Node rt;     // the right/top subtree

        private Node(Point2D point, RectHV rect) {
            this.p = point;
            this.rect = rect; }
        private boolean compareToX(Point2D point) { return this.p.x() > point.x(); }
        private boolean compareToY(Point2D point) { return this.p.y() > point.y(); }
    }

    // is the set empty?
    public boolean isEmpty() { return size() == 0; }

    // number of points in the set
    public int size() { return N; }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D point) {
        if (isEmpty()) {
            RectHV rect = new RectHV(0, 0, 1, 1);
            root = new Node(point, rect);
            N++;
            return;
        }
        insertBlue(root, point);
    }
    private void insertBlue(Node node, Point2D point) {
        if (node.p.compareTo(point) == 0) return;
        if (node.compareToX(point)) {
            if (node.rt == null) {
                RectHV rect = new RectHV(node.rect.xmin(), node.rect.ymin(),
                                node.p.x(), node.rect.ymax());
                node.rt = new Node(point, rect);
                N++;
                return;
            }
            insertRed(node.rt, point);
        } else {
            if (node.lb == null) {
                RectHV rect = new RectHV(node.p.x(), node.rect.ymin(),
                                node.rect.xmax(), node.rect.ymax());
                node.lb = new Node(point, rect);
                N++;
                return;
            }
            insertRed(node.lb, point);
        }
    }
    private void insertRed(Node node, Point2D point) {
        if (node.p.compareTo(point) == 0) return;
        if (node.compareToY(point)) {
            if (node.rt == null) {
                RectHV rect = new RectHV(node.rect.xmin(), node.rect.ymin(),
                                node.rect.xmax(), node.p.y());
                node.rt = new Node(point, rect);
                N++;
                return;
            }
            insertBlue(node.rt, point);
        } else {
            if (node.lb == null) {
                RectHV rect = new RectHV(node.rect.xmin(), node.p.y(),
                                node.rect.xmax(), node.rect.ymax());
                node.lb = new Node(point, rect);
                N++;
                return;
            }
            insertBlue(node.lb, point);
        }
    }

    // does the set contain point p?
    public boolean contains(Point2D point) {
        return !isEmpty() && contains(root, point); }
    private boolean contains(Node node, Point2D point) {
        if (node.p.compareTo(point) == 0) return true;
        if (node.rt != null && node.rt.rect.contains(point))
            if (contains(node.rt, point)) return true;
        if (node.lb != null && node.lb.rect.contains(point))
            if (contains(node.lb, point)) return true;
        return false;
    }

    // draw all points to standard draw
    public void draw() { drawRed(root); }
    private void drawRed(Node node) {
        if (node == null) return;
        // draw the points
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(.01);
        node.p.draw();
        // draw the separator
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.setPenRadius();
        StdDraw.line(node.p.x(), node.rect.ymin(), node.p.x(), node.rect.ymax());
        // draw sub-nodes
        drawBlue(node.lb);
        drawBlue(node.rt);
    }
    private void drawBlue(Node node) {
        if (node == null) return;
        // draw the points
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(.01);
        node.p.draw();
        // draw the separator
        StdDraw.setPenColor(StdDraw.BLUE);
        StdDraw.setPenRadius();
        StdDraw.line(node.rect.xmin(), node.p.y(), node.rect.xmax(), node.p.y());
        // draw sub-nodes
        drawRed(node.lb);
        drawRed(node.rt);
    }

    // all points that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        if (isEmpty()) return new Stack<Point2D>();
        return range(new Stack<Point2D>(), root, rect);
    }
    private Iterable<Point2D> range(Stack<Point2D> range, Node node, RectHV rect) {
        if (node.rt != null && node.rt.rect.intersects(rect))
            range(range, node.rt, rect);
        if (node.lb != null && node.lb.rect.intersects(rect))
            range(range, node.lb, rect);
        if (rect.contains(node.p))
            range.push(node.p);
        return range;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D point) {
        if (isEmpty()) return null;
        return nearest(root, Double.MAX_VALUE, null, point);
    }
    private Point2D nearest(Node node, double nd, Point2D np, Point2D point) {
        Point2D cp = node.p;
        double cd = cp.distanceSquaredTo(point);
        if (cd > nd) {
            cd = nd;
            cp = np;
        }

        if (node.rt != null && node.rt.rect.distanceSquaredTo(point) < cd) {
            if (node.lb != null && node.lb.rect.distanceSquaredTo(point) < cd) {
                if (node.rt.rect.contains(point)) {
                    cp = nearest(node.rt, cd, cp, point);
                    return nearest(node.lb, cp.distanceSquaredTo(point), cp, point);
                } else {
                    cp = nearest(node.lb, cd, cp, point);
                    return nearest(node.rt, cp.distanceSquaredTo(point), cp, point);
                }
            } else
                return nearest(node.rt, cd, cp, point);
        }
        else if (node.lb != null && node.lb.rect.distanceSquaredTo(point) < nd)
            return nearest(node.lb, cd, cp, point);
        return cp;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        StdDraw.clear();
        KdTree kdTree = new KdTree();

        In in = new In(args[0]);
        while (!in.isEmpty()) {
            double x = in.readDouble();
            double y = in.readDouble();
            Point2D p = new Point2D(x, y);
            kdTree.insert(p);
        }

        kdTree.draw();
    }
}
