public class PointSET {
    private SET<Point2D> pSET;

    // construct an empty set of points
    public PointSET() { pSET = new SET<Point2D>(); }

    // is the set empty?
    public boolean isEmpty() { return pSET.isEmpty(); }

    // number of points in the set
    public int size() { return pSET.size(); }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) { pSET.add(p); }

    // does the set contain point p?
    public boolean contains(Point2D p) { return pSET.contains(p); }

    // draw all points to standard draw
    public void draw() {
        for (Point2D point2D : pSET)
            StdDraw.point(point2D.x(), point2D.y());
    }

    // all points that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        Stack<Point2D> points = new Stack<Point2D>();
        for (Point2D point : pSET)
            if (rect.contains(point))
                points.push(point);
        return points;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (isEmpty()) return null;
        Point2D pMin = pSET.min();
        double dMin = p.distanceSquaredTo(pMin);
        for (Point2D point : pSET)
            if (p.distanceSquaredTo(point) < dMin) {
                pMin = point;
                dMin = p.distanceSquaredTo(pMin);
            }
        return pMin;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        StdDraw.clear();
        StdDraw.setPenColor(StdDraw.BOOK_BLUE);
        StdDraw.setPenRadius(.005);

        PointSET pSet = new PointSET();

        In in = new In(args[0]);
        while (!in.isEmpty()) {
            double x = in.readDouble();
            double y = in.readDouble();
            Point2D p = new Point2D(x, y);
            pSet.insert(p);
        }

        pSet.draw();
    }
}