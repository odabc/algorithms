public class Ball {
    private double rx, ry;
    private double vx, vy;
    private double radius;

    public Ball() {
        rx = StdRandom.uniform(0, 1.0);
        vx = StdRandom.uniform(-0.075, 0.075);
        ry = StdRandom.uniform(0, 1.0);
        vy = StdRandom.uniform(-0.075, 0.075);
        radius = StdRandom.uniform(0.005, 0.025);
    }

    public void move(double dt) {
        if ((rx + vx*dt >= 1) || (rx + vx*dt <= 0)) { vx = -vx; }
        if ((ry + vy*dt >= 1) || (ry + vy*dt <= 0)) { vy = -vy; }
        rx += vx*dt;
        ry += vy*dt;
    }

    public void draw() { StdDraw.filledCircle(rx, ry, radius); }
}
