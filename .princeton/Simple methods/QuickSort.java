//import java.io.BufferedReader;
//import java.io.InputStreamReader;

public class QuickSort
{
    public static void main(String[] args) throws Exception
    {
        int[] numbers = {9, 4, 7, 9, 8}; //new int[5];
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        for (int i = 0; i < 5; i++) {
//            numbers[i] = Integer.parseInt(reader.readLine());
//        }
        qsort(numbers, 0, 4);
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        //Напишите тут ваш код
    }
    public static void qsort(int[] array, int l, int r) {
        int i = l;
        int j = r+1;
        int x = array[l];
        while (true) {
            while (array[++i] < x) if (i == r) break;
            while (array[--j] >= x) if (j == l) break;

            if (i >= j) break;
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        int temp = array[l];
        array[l] = array[j];
        array[j] = temp;

        if (j+1 < r) {
            qsort(array, j+1, r);
        }
        if (l < j-1) {
            qsort(array, l, j-1);
        }
    }
}
