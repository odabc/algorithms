public class SubtitlesSep {
    public static void main(String[] args) {
        StdOut.print("Enter file name: ");
        String name = StdIn.readLine();
        In in = new In(name + ".srt");    // input file
        Out out = new Out(name + ".txt"); // output file

        while (!in.isEmpty()) {
            String a = "";
            in.readLine();
            in.readLine();
            if (!in.isEmpty())
                a = in.readLine();
            if (!in.isEmpty())
                a += " " + in.readLine();
            out.println(a);
            if (!in.isEmpty())
                out.println(in.readLine());
        }
        in.close();
        out.close();
    }
}
