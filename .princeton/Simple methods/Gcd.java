// Euclid method calculaton greatest common divisor
public class Gcd {

    // greatest common divisor
    public static int gcd(int a, int b) {
        if (b == 0) return a;
        int c = a % b;
        // a < b swap a and b (c = a)
        // a > b -> find gcd for a and modulo a / b
        return gcd(b, c);
    }

    public static void main(String[] args) {
        StdOut.println("Enter two digits:");
        int a = StdIn.readInt();
        int b = StdIn.readInt();
        StdOut.println("Greatest common divisor = " + gcd(a, b));

    }
}
