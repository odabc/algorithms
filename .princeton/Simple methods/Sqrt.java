public class Sqrt {
    public static double sqrt(double a) {
        if (a < 0) return Double.NaN;
        double err = 10e-15;
        double t = a;

        double d = a;
        while (d > t * err) {
            // new iteration (res_hi + res_lo) / 2
            t = (a/t + t)/2;
            // delta
            d = t - a/t;
            // mod(d)
            if (d < 0) d = -d;
        }
        return t;
    }

    public static void main(String[] args) {
        StdOut.print("Enter double number: ");
        double a = StdIn.readDouble();
        StdOut.println("Square root is equal: " + sqrt(a));
    }
}
