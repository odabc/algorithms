import java.awt.Color;

public class SeamCarver {
    private int[][] colors;
    private int width;
    private int height;
    private boolean isTransposing;

    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        this.width = picture.width();
        this.height = picture.height();
        this.colors = new int[height][width];
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                this.colors[y][x] = picture.get(x, y).getRGB();
        isTransposing = false;
    }
    // current picture
    public Picture picture() {
        revert();
        Picture result = new Picture(width, height);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                result.set(x, y, new Color(colors[y][x]));
        return result;
    }
    // width of current picture
    public int width() {
        if (!isTransposing)
            return width;
        return height;
    }
    // height of current picture
    public int height() {
        if (!isTransposing)
            return height;
        return width;
    }

    // energy of pixel at column x and row y
    public double energy(int x, int y) {
        revert();
        return energyXY(x, y);
    }
    private double energyXY(int x, int y) {
        if (x < 0 || x >= width || y < 0 || y >= height)
            throw new IndexOutOfBoundsException();
        if (x == 0 || x == width-1 || y == 0 || y == height-1)
            return 195075; // 255 * 255 + 255 * 255 + 255 * 255
        return energyX(x, y) + energyY(x, y);
    }
    private double energyX(int x, int y) {
        Color x1 = new Color(colors[y][x-1]);
        Color x2 = new Color(colors[y][x+1]);
        return (x1.getRed()-x2.getRed())*(x1.getRed()-x2.getRed())
                + (x1.getGreen()-x2.getGreen()) * (x1.getGreen()-x2.getGreen())
                + (x1.getBlue()-x2.getBlue()) * (x1.getBlue()-x2.getBlue());
    }
    private double energyY(int x, int y) {
        Color x1 = new Color(colors[y-1][x]);
        Color x2 = new Color(colors[y+1][x]);
        return (x1.getRed()-x2.getRed())*(x1.getRed()-x2.getRed())
                + (x1.getGreen()-x2.getGreen()) * (x1.getGreen()-x2.getGreen())
                + (x1.getBlue()-x2.getBlue()) * (x1.getBlue()-x2.getBlue());
    }

    // sequence of indices for horizontal seam
    public int[] findHorizontalSeam() {
        transpose();
        return findSeam();
    }
    // sequence of indices for vertical seam
    public int[] findVerticalSeam() {
        revert();
        return findSeam();
    }
    private int[] findSeam() {
        double[][] distTo = new double[height][width];
        int[][] edgeTo = new int[height][width];
        double[][] energy = new double[height][width];

        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {
                distTo[y][x] = Double.POSITIVE_INFINITY;
                energy[y][x] = energyXY(x, y);
            }

        for (int x = 1; x < width-1; x++)
            distTo[0][x] = 0.0;

        for (int y = 1; y < height; y++)
            for (int x = 1; x < width-1; x++)
                for (int i = -1; i <= 1; i++) {
                    double dist = distTo[y -1][x]+energy[y-1][x];
                    if (distTo[y][x+i] > dist) {
                        distTo[y][x + i] = dist;
                        edgeTo[y][x + i] = x;
                    }
                }

        int[] result = new int[height];
        int pos = 0;
        double min = distTo[height-1][pos];
        for (int x = 1; x < width-1; x++) {
            if (distTo[height-1][x] < min) {
                min = distTo[height-1][x];
                pos = x;
            }
        }
        for (int y = height-1; y >= 0; y--) {
            result[y] = pos;
            pos = edgeTo[y][pos];
        }
        return result;
    }

    // remove horizontal seam from current picture
    public void removeHorizontalSeam(int[] seam) {
        transpose();
        removeSeam(seam);
    }
    // remove vertical seam from current picture
    public void removeVerticalSeam(int[] seam) {
        revert();
        removeSeam(seam);
    }
    private void removeSeam(int[] seam) {
        if (seam == null)
            throw new NullPointerException();
        if (seam.length != height)
            throw new IllegalArgumentException();
        width--;
        for (int y = 0; y < height; y++)
            System.arraycopy(colors[y], seam[y]+1,
                    colors[y], seam[y], width-seam[y]);
    }

    private void revert() { rotate(false); }
    private void transpose() { rotate(true); }
    private void rotate(boolean transpose) {
        if (this.isTransposing == transpose) return;
        this.isTransposing = false;
        int[][] p = new int[width][height];
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                p[x][y] = this.colors[y][x];
        this.colors = p;
        int tmp = this.width;
        this.width = this.height;
        this.height = tmp;
        this.isTransposing = transpose;
    }
}

