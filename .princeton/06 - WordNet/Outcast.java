public class Outcast {
    private WordNet wordnet;

    // constructor takes a WordNet object
    public Outcast(WordNet wordnet) {
        this.wordnet = wordnet;
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns) {
        int[] relation = new int[nouns.length];
        for (int i = 0; i <= nouns.length; i++) {
            for (int j = nouns.length-1; j > i; j--) {
                int sum = wordnet.distance(nouns[i], nouns[j]);
                if (sum > 0) {
                    relation[i] += sum;
                    relation[j] += sum;
                }
            }
        }

        int ind = 0;
        for (int i = 1; i < nouns.length; i++)
            if (relation[ind] < relation[i]) ind = i;

        return nouns[ind];
    }

    // see test client below
    public static void main(String[] args) {
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
    }
}