import java.util.Set;
import java.util.HashSet;

public class SAP {
    // two queues for bfs
    private final int[] QV;
    private int topQV;
    private int bottomQV;
    private final int[] QW;
    private int topQW;
    private int bottomQW;
    // software indexing stack for clearing
    private int markI;
    private final int[] mark;
    // boolean and int array for dfs
    private final boolean[] markV;
    private final boolean[] markW;
    private final int[] distV;
    private final int[] distW;
    // soft cache path for sequence point
    private final Set<Integer> pathV = new HashSet<Integer>();
    private final Set<Integer> pathW = new HashSet<Integer>();
    // soft cache for two point and switch for select sequence or point
    private boolean simple;
    private int simpleV;
    private int simpleW;
    // storing result and Graph
    private int pathL;
    private int pathA;
    private Digraph G;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        this.G = new Digraph(G.V());
        for (int v = 0; v < G.V(); v++)
            for (int w: G.adj(v))
                this.G.addEdge(v, w);

        distV = new int[G.V()];
        distW = new int[G.V()];
        QV = new int[G.V()];
        QW = new int[G.V()];
        mark = new int[2*G.V()+1];
        markV = new boolean[G.V()];
        markW = new boolean[G.V()];
        reset();

        simpleV = -1;
        simpleW = -1;
        simple = false;
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        if (simpleV != v || simpleW != w || !simple) {
            simpleV = v;
            simpleW = w;
            simple = true;
            connected(v, w);
        }
        return pathL;
    }

    // a common ancestor of v and w that participates in a shortest ancestral path;
    // -1 if no such path
    public int ancestor(int v, int w) {
        if (simpleV != v || simpleW != w || !simple) {
            simpleV = v;
            simpleW = w;
            simple = true;
            connected(v, w);
        }
        return pathA;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w;
    // -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        simple = false;
        int size = pathV.size();
        for (int x: v)
            if (pathV.contains(x)) size--;
            else {
                connected(v, w);
                return pathL;
            }
        if (size != 0) {
            connected(v, w);
            return pathL;
        }
        size = pathW.size();
        for (int y: w)
            if (pathW.contains(y)) size--;
            else {
                connected(v, w);
                return pathL;
            }
        if (size != 0)
            connected(v, w);
        else if (pathA == -1) pathL = -1;
        return pathL;
    }

    // a common ancestor that participates in shortest ancestral path;
    // -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        simple = false;
        int size = pathV.size();
        for (int x: v)
            if (pathV.contains(x)) size--;
            else {
                connected(v, w);
                return pathA;
            }
        if (size != 0) {
            connected(v, w);
            return pathA;
        }
        size = pathW.size();
        for (int y: w)
            if (pathW.contains(y)) size--;
            else {
                connected(v, w);
                return pathA;
            }
        if (size != 0)
            connected(v, w);
        return pathA;
    }

    private boolean connected(Iterable<Integer> v, Iterable<Integer> w) {
        reset();

        //init queue bfs for sets vertices
        for (int x: v) {
            if (x < 0 || x >= G.V())
                throw new IndexOutOfBoundsException();
            pathV.add(x);
            markV[x] = true;
            mark[markI++] = x;
            distV[x] = 0;
            QV[topQV++] = x;
        }

        for (int y: w) {
            if (y < 0 || y >= G.V())
                throw new IndexOutOfBoundsException();
            pathW.add(y);
            if (!markV[y]) {
                markW[y] = true;
                mark[markI++] = y;
                distW[y] = 0;
                QW[topQW++] = y;
            } else {
                pathA = y;
                pathL = distV[y];
            }
        }
        if (pathA != -1) return true;

        bfs();
        return pathA != -1;
    }

    private boolean connected(int v, int w) {
        reset();

        if (v < 0 || v >= G.V() || w < 0 || w >= G.V()) {
            simpleV = -1;
            simpleW = -1;
            simple = false;
            throw new IndexOutOfBoundsException();
        }

        //init queue bfs for sets vertices
        pathV.add(v);
        markV[v] = true;
        mark[markI++] = v;
        distV[v] = 0;
        QV[topQV++] = v;

        pathW.add(w);
        if (!markV[w]) {
            markW[w] = true;
            mark[markI++] = w;
            distW[w] = 0;
            QW[topQW++] = w;
        } else {
            pathA = v;
            pathL = 0;
            return true;
        }

        bfs();
        return pathA != -1;
    }

    private void reset() {
        topQV = 0;
        bottomQV = 0;
        topQW = 0;
        bottomQW = 0;
        pathV.clear();
        pathW.clear();

        // init array for search path
        while (markI > 0) {
            markI--;
            markV[mark[markI]] = false;
            markW[mark[markI]] = false;
        }

        pathL = Integer.MAX_VALUE;
        pathA = -1;
    }

    // two direction bfs with pre-initializing queries
    private void bfs() {
        int lwl = 0;
        while ((topQV > bottomQV || topQW > bottomQW) && pathL >= ++lwl) {
            while (topQV > bottomQV) {
                int x = QV[bottomQV++];
                if (markW[x]) {
                    int tmp = distV[x] + distW[x];
                    if (pathL > tmp) {
                        pathA = x;
                        pathL = tmp;
                    }
                }

                for (int y : G.adj(x))
                    if (!markV[y]) {
                        distV[y] = distV[x] + 1;
                        markV[y] = true;
                        mark[markI++] = y;
                        QV[topQV++] = y;
                    }

                if (lwl <= distV[x]) break;
            }
            while (topQW > bottomQW) {
                int x = QW[bottomQW++];
                if (markV[x]) {
                    int tmp = distV[x] + distW[x];
                    if (pathL > tmp) {
                        pathA = x;
                        pathL = tmp;
                    }
                }

                for (int y : G.adj(x))
                    if (!markW[y]) {
                        distW[y] = distW[x] + 1;
                        markW[y] = true;
                        mark[markI++] = y;
                        QW[topQW++] = y;
                    }

                if (lwl <= distW[x]) break;
            }
        }
        if (pathA == -1) pathL = -1;
    }

    // do unit testing of this class
    public static void main(String[] args) {
        In in = new In(args[0]);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length   = sap.length(v, w);
            int ancestor = sap.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}