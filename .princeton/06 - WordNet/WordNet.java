import java.util.Map;
import java.util.HashMap;

public class WordNet {
    private final Digraph G;
    private final SAP sap;
    private final Map<String, Bag<Integer>> nouns
            = new HashMap<String, Bag<Integer>>();
    private String[] ids;

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        this.G = new Digraph(initNouns(synsets));
        initGraph(hypernyms);
        this.sap = new SAP(G);
        IsDAG good = new IsDAG();
        if (!good.isChecked())
            throw new IllegalArgumentException();
    }
    private int initNouns(String synsets) {
        In inNoun = new In(synsets);
        Map<Integer, String> idsBuf = new HashMap<Integer, String>();
        while (!inNoun.isEmpty()) {
            String[] noun = inNoun.readLine().split(",");
            int id = Integer.parseInt(noun[0]);
            String[] synset = noun[1].split(" ");
            idsBuf.put(id, noun[1]);
            for (String st: synset) {
                Bag<Integer> idBag;
                if (nouns.containsKey(st))
                    idBag = nouns.get(st);
                else idBag = new Bag<Integer>();
                idBag.add(id);
                nouns.put(st, idBag);
            }
        }

        ids = new String[idsBuf.size()];
        for (int i = 0; i < ids.length; i++)
            ids[i] = idsBuf.get(i);
        return ids.length;
    }
    private void initGraph(String hypernyms) {
        In inGraph = new In(hypernyms);
        while (!inGraph.isEmpty()) {
            String[] relation = inGraph.readLine().split(",");
            int v = Integer.parseInt(relation[0]);
            for (int i = 1; i < relation.length; i++) {
                int w = Integer.parseInt(relation[i]);
                if (v >= G.V() || w >= G.V())
                    throw  new IllegalArgumentException();
                this.G.addEdge(v, w);
            }
        }
    }

    private class IsDAG {
        private boolean[] marked;
        private boolean[] onStack;
        private boolean result;
        private int root;

        public IsDAG() {
            marked = new boolean[G.V()];
            onStack = new boolean[G.V()];
            root = -1;
            result = isDAG();
        }
        public boolean isChecked() { return result; }

        private boolean isDAG() {
            for (int v = 0; v < G.V(); v++)
                if (!marked[v] && cycles(v)) return false;

            marked = new boolean[G.V()];
            dfs(root, G.reverse());
            for (boolean mark: marked)
                 if (!mark) return false;
            return true;
        }
        private boolean cycles(int v) {
            marked[v] = true;
            onStack[v] = true;
            for (int w: G.adj(v))
                if (!marked[w] && !result)
                    result = cycles(w);
                else if (onStack[w])
                    return true;
            onStack[v] = false;
            if (root == -1) root = v;
            return result;
        }
        private void dfs(int v, Digraph rG) {
            marked[v] = true;
            for (int w: rG.adj(v))
                if (!marked[w])
                    dfs(w, rG);
        }
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() { return nouns.keySet(); }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        if (word == null)
            throw new java.lang.NullPointerException();
        return nouns.containsKey(word);
    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        if (!nouns.containsKey(nounA) || !nouns.containsKey(nounB)) {
            if (nounA == null || nounB == null)
                throw new NullPointerException();
            throw new IllegalArgumentException();
        }
        Bag<Integer> vIDs = nouns.get(nounA);
        Bag<Integer> wIDs = nouns.get(nounB);
        return sap.length(vIDs, wIDs);
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA
    // and nounB in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        if (!nouns.containsKey(nounA) || !nouns.containsKey(nounB)) {
            if (nounA == null || nounB == null)
                throw new NullPointerException();
            throw new IllegalArgumentException();
        }
        Bag<Integer> vIDs = nouns.get(nounA);
        Bag<Integer> wIDs = nouns.get(nounB);
        return ids[(sap.ancestor(vIDs, wIDs))];
    }

    // do unit testing of this class
    public static void main(String[] args) {
        WordNet wn = new WordNet(args[0], args[1]);

        StdOut.printf("nouns: %10s\n", wn.nouns.size());
        StdOut.printf("vertices: %7s\n", wn.G.V());
        StdOut.printf("edges: %10s\n", wn.G.E());

//        String nounA = "Spanish_War";
//        String nounB = "baryon_number";

        String nounA = "Black_Plague";
        String nounB = "black_marlin";

        wn.isNoun(nounA);
        wn.nouns();

        Stopwatch timer = new Stopwatch();
        for (int i = 0; i < 250000; i++) {
            wn.distance(nounA, nounB);
            wn.sap(nounB, nounA);
        }
        StdOut.printf("Distance between %s and %s is %d\n",
                nounA, nounB, wn.distance(nounA, nounB));
        StdOut.printf("Common ancestor %s and %s is %s\n",
                nounA, nounB, wn.sap(nounA, nounB));
        StdOut.println("Time: " + timer.elapsedTime());
    }
}