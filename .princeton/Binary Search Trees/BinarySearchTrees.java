public class BinarySearchTrees<Key extends Comparable<Key>, Value> {
    private Node root;

    private class Node {
        private Key key;
        private Value val;
        private Node left, right;
        private int N;
        public Node(Key key, Value val, int N) {
            this.key = key;
            this.val = val;
            this.N = N;
        }
    }

    public boolean isEmpty() { return size() == 0; }
    public int size() { return size(root); }
    private int size(Node x) {
        if (x == null) return 0;
        return x.N;
    }

    public boolean contains(Key key) {
        return get(key) != null;
    }
    public Value get(Key key) {
        Node x = root;
        while (x != null) {
            int cmp = key.compareTo(x.key);
            if (cmp < 0)
                x = x.left;
            else if (cmp > 0)
                x = x.right;
            else
                return x.val;
        }
        return null;
    }

    public void put(Key key, Value val) {
        if (val == null) {
//            delete(key);
            return;
        }
        if (root == null) {
            root = new Node(key, val, 1);
            return;
        }
        Node x = root;
        boolean contains = contains(key);
        while (x != null) {
            int cmp = key.compareTo(x.key);
            if (cmp < 0) {
                if (!contains) x.N++;
                if (x.left == null) {
                    x.left = new Node(key, val, 1);
                    return;
                }
                x = x.left;
            } else if (cmp > 0) {
                if (!contains) x.N++;
                if (x.right == null) {
                    x.right = new Node(key, val, 1);
                    return;
                }
                x = x.right;
            } else {
                x.val = val;
                return;
            }
        }
    }

    public Iterable<Key> levelOrder() {
        Queue<Key> keys = new Queue<Key>();
        Queue<Node> queue = new Queue<Node>();
        queue.enqueue(root);
        while (!queue.isEmpty()) {
            Node x = queue.dequeue();
            if (x == null) continue;
            keys.enqueue(x.key);
            queue.enqueue(x.left);
            queue.enqueue(x.right);
        }
        return keys;
    }

    public static void main(String[] args) {
        BinarySearchTrees<String, Integer> st = new BinarySearchTrees<String, Integer>();
        for (int i = 0; !StdIn.isEmpty(); i++) {
            String key = StdIn.readString();
            st.put(key, i);

            for (String s : st.levelOrder())
                StdOut.println(s + " " + st.get(s));
        }


    }

}
