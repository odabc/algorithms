public class Insertion extends Sorting {

    public static void sort(Comparable[] a) { sort(a, NO_DRAW); }
    public static void sort(Comparable[] a, boolean draw) {
        int N = a.length;
        for (int i = 1; i < N; i++) {
            int j = i;
            Comparable tmp = a[i];
            for (; j > 0 && less(tmp, a[j - 1]); j--)
                a[j] = a[j-1];
            if (i-j > 0) a[j] = tmp;

//            for (int j = i; j > 0 && less(a[j], a[j-1]); j--)
//                exch(a, j, j-1);

            if (draw) draw("Insertion", a, i);
        }
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int[] tmp = in.readAllInts();
        Integer[] a = new Integer[tmp.length];
        for (int i = 0; i < tmp.length; i++)
            a[i] = tmp[i];

        Insertion.sort(a, DRAW);
        assert Insertion.isSorted(a) : "Error in sorting";
        Insertion.show(a);
    }
}
