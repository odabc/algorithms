import java.util.Arrays;

public class SortCompare {
    public static double time(String alg, Comparable[] a) {
        Stopwatch timer = new Stopwatch();
        if (alg.equals("Insertion")) {
            Insertion.sort(a);
        }
        if (alg.equals("Selection")) {
            Selection.sort(a);
        }
        if (alg.equals("Shell")) {
            Shell.sort(a);
        }
        if (alg.equals("Merge Down")) {
            MergeD.sort(a);
        }
        if (alg.equals("Merge Up")) {
            MergeU.sort(a);
        }
        if (alg.equals("Quick")) {
            Quick.sort(a);
        }
        if (alg.equals("System")) {
            Arrays.sort(a);
        }
        return timer.elapsedTime();
    }

    public static double timeRandomInput(String alg, int N, int T) {
        double total = 0.0;
        Integer[] a = new Integer[N];
//        In in = new In("8kInts.txt");
//        int[] tmp = in.readAllInts();
//        Integer[] a = new Integer[tmp.length];
//        for (int i = 0; i < tmp.length; i++)
//            a[i] = tmp[i];

        for (int t = 0; t < T; t++) {
            for (int i = 0; i < N; i++)
                a[i] = StdRandom.uniform(-1000000, 1000000);

            total += time(alg, a);
        }
        return total;
    }

    public static void main(String[] args) {
        String alg1 = "Quick";
        String alg2 = "System";
        int N = 10000;
        int T = 1000;

        double t1 = timeRandomInput(alg1, N, T);
        double t2 = timeRandomInput(alg2, N, T);

        StdOut.printf("Для %d случайных чисел\n     %s в ", N, alg1);
        StdOut.printf(" %.2f раз быстрее, чем %s\n", t2/t1, alg2);
    }
}
