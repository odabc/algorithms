public abstract class Sorting {
    public static final boolean DRAW = true;
    public static final boolean NO_DRAW = false;

    public static void sort(Comparable[] a) { sort(a, NO_DRAW); }
    public static void sort(Comparable[] a, boolean draw) {
        throw new IllegalAccessError("Function not defended."); }

    public static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0; }

    public static void exch(Comparable[] a, int i, int j) {
        Comparable swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    public static boolean isSorted(Comparable[] a) {
        return isSorted(a, 0, a.length-2); }
    public static boolean isSorted(Comparable[] a, int lo, int hi) {
        for (int i = lo+1; i <= hi; i++)
            if (less(a[i], a[i-1])) return false;
        return true;
    }

    public static void show(Comparable[] a) {
        for (Comparable e : a)
            StdOut.print(e + " ");
        StdOut.println();
    }

    public static void subSort(Comparable[] a, int lo, int hi) {
        subSort(a, NO_DRAW, lo, hi); }
    public static void subSort(Comparable[] a, boolean draw, int lo, int hi) {
        for (int i = lo+1; i <= hi; i++) {
            int j = i;
            Comparable tmp = a[i];
            for (; j > lo && less(tmp, a[j - 1]); j--)
                a[j] = a[j-1];
            if (i-j > 0) a[j] = tmp;

            if (draw) draw("Small part insert", a, i);
        }
    }

    public static void draw(Comparable[]a) { draw("", a, a.length); }
    public static void draw(String nm, Comparable[]a) { draw(nm, a, a.length); }
    public static void draw(String nm, Comparable[] a, int i) {
        int N = a.length;
        // rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, N);
        StdDraw.setYscale(0, 2000);
//        StdDraw.setPenRadius(0.5);

        StdDraw.clear();
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.textLeft(10, 1950, nm + " sort.");
        StdDraw.setPenColor(StdDraw.BOOK_BLUE);
        for (int k = 0; k <= i; k++)
            StdDraw.filledRectangle(k, 0, 1, 1000
                    + Integer.parseInt(a[k].toString()) / 1000);

        StdDraw.setPenColor(StdDraw.BOOK_RED);
        for (int k = i + 1; k < N; k++)
            StdDraw.filledRectangle(k, 0, 1, 1000
                    + Integer.parseInt(a[k].toString()) / 1000);
        StdDraw.show(0);
    }
}
