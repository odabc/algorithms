public class Quick extends Sorting {
    private static int CUTOFF = 10;

    // main recursion call
    public static void sort(Comparable[] a) { sort(a, NO_DRAW); }
    public static void sort(Comparable[] a, boolean draw) {
        sort(a, draw, 0, a.length-1);
    }

    // sub-recursion call -> sorting a[lo...hi]
    public static void sort(Comparable[] a, boolean draw, int lo, int hi) {
//        if (hi <= lo) return;
        if (hi <= lo + CUTOFF) {
            subSort(a, draw, lo, hi);
            return;
        }

//        int p = partition(a, draw, lo, hi);
//        sort(a, draw, lo, p-1);
//        sort(a, draw, p+1, hi);
//        assert isSorted(a, lo, p-1) : "Error in sorting part left";
//        assert isSorted(a, p+1, hi) : "Error in sorting part right";

        // 3-way improvement
        int lt = lo, gt = hi;
        int i = lo+1;
        Comparable v = a[lo];

        while (i <= gt) {
            int cmp = a[i].compareTo(v);
            if      (cmp < 0)   exch(a, lt++, i++);
            else if (cmp > 0)   exch(a, i, gt--);
            else                i++;

            if (draw) draw("Quick", a, i);
        }

        // now a[lo...lt-1] < v = a[lt...gt] < a[gt+1...hi]
        sort(a, draw, lo, lt-1);
        sort(a, draw, gt+1, hi);
        assert isSorted(a, lo, lt-1) : "Error in sorting part left";
        assert isSorted(a, gt+1, hi) : "Error in sorting part right";
    }

    public static int partition(Comparable[] a, boolean draw, int lo, int hi) {
        int i = lo, j = hi + 1;
        Comparable v = a[lo];

        while (true) {
            while (less(a[++i], v)) if (i == hi) break;
            while (less(v, a[--j])) if (j == lo) break;

            if (i >= j) break;
            exch(a, i, j);
            if (draw) draw("Quick", a, i);
        }

        exch(a, lo, j);
        return j;
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int[] tmp = in.readAllInts();
        Integer[] a = new Integer[tmp.length];
        for (int i = 0; i < tmp.length; i++)
            a[i] = tmp[i];

        Quick.sort(a, DRAW);
        assert Quick.isSorted(a) : "Error in sorting";
        Quick.show(a);
    }
}
