public class Heap extends Sorting {

    public static void sort(Comparable[] a) { sort(a, NO_DRAW); }
    public static void sort(Comparable[] a, boolean draw) {
        int N = a.length-1;
        // make binary heap
        for (int k = (N-1)/2; k >= 0; k--) {
            sink(a, k);
            if (draw) draw("Heap", a, k);
        }

        // find max, insert it on last pos and decrease size heap
        while (N > 0) {
            exch(a, 0, N--);
            sink(a, 0, N);
            if (draw) draw("Heap", a, a.length-N-1);
        }
        assert Heap.isSorted(a) : "Error in sorting";
    }

    private static void swim(Comparable[] a, int k) {
        // if father < our child -> exchange
        while (k > 0 && less(a[(k-1)/2], a[k])) {
            exch(a, (k-1)/2, k);
            k = (k-1)/2;
        }
    }

    private static void sink(Comparable[] a, int k) { sink(a, k, a.length-1); }
    private static void sink(Comparable[] a, int k, int N) {
        while (2*k+1 <= N) {
            int j = 2*k+1;
            // find larger child {2*k, 2*k+1}
            if (j < N && less(a[j], a[j+1])) j++;
            // check parents > lager child
            if (!less(a[k], a[j])) break;
            exch(a, k, j);
            k = j;
        }
    }


    public static void main(String[] args) {
        In in = new In(args[0]);
        int[] tmp = in.readAllInts();
        Integer[] a =  new Integer[tmp.length];
        for (int i = 0; i < tmp.length; i++)
            a[i] = tmp[i];

        Heap.sort(a, DRAW);
        assert Heap.isSorted(a) : "Error in sorting";
        Heap.show(a);
    }
}
