public class MergeU extends Sorting {
    private static Comparable[] aux;
    private static int CUTOFF = 7;

    // merge two arrays a[lo...mid] & a[mid+1...hi]
    public static void merge(Comparable[] a, int lo, int mid, int hi) {
        assert isSorted(a, lo, mid) : "Error in sorting part left";
        assert isSorted(a, mid+1, hi) : "Error in sorting part right";
        int i = lo, j = mid+1;
        // copy a[lo...hi] to aux[lo...hi]
        System.arraycopy(a, lo, aux, lo, hi-lo+1);
        // merge back with compare
        for (int k = lo; k <= hi; k++) {
            if      (i > mid)               a[k] = aux[j++];
            else if (j > hi)                a[k] = aux[i++];
            else if (less(aux[i], aux[j]))  a[k] = aux[i++];
            else                            a[k] = aux[j++];
        }
        assert isSorted(a, lo, hi) : "Error in merge";
    }

    public static void sort(Comparable[] a) { sort(a, NO_DRAW); }
    public static void sort(Comparable[] a, boolean draw) {
        int N = a.length;
        aux = new Comparable[N];
        if (N > CUTOFF)
            for (int i = 0; i < N; i += CUTOFF) {
                int hi = i + CUTOFF - 1;
                if (hi >= N) hi = N - 1;
                subSort(a, draw, i, hi);
            }
        else subSort(a, draw, 0, N-1);

        for (int sz = CUTOFF; sz < N; sz = sz+sz)
            for (int lo = 0; lo < N-sz; lo += sz+sz) {
                int hi;
                if (lo + sz + sz < N) hi = lo + sz + sz - 1;
                else hi = N - 1;
                merge(a, lo, lo + sz - 1, hi);
                if (draw) draw("Merge up", a, hi);
            }
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int[] tmp = in.readAllInts();
        Integer[] a = new Integer[tmp.length];
        for (int i = 0; i < tmp.length; i++)
            a[i] = tmp[i];

        MergeU.sort(a, DRAW);
        assert MergeU.isSorted(a) : "Error in sorting";
        MergeU.show(a);
    }
}

