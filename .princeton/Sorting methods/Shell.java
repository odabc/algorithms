public class Shell extends Sorting {

    public static void sort(Comparable[] a) { sort(a, NO_DRAW); }
    public static void sort(Comparable[] a, boolean draw) {
        int N = a.length;
        int lim = N/3;
        int h = 1;
        // 1, 4, 13, 40, 121, 364, 1093, ...
        while (h < lim) h = h*3 + 1;

        while (h >= 1) {
            for (int i = h; i < N; i++) {
                int j = i;
                Comparable tmp = a[i];
                // Insertion a[i] between a[i-h], a[i-2*h], a[i-3h] ... .
                for (; j >= h && less(tmp, a[j-h]); j -= h)
                    a[j] = a[j-h];
                if (i - j > 0) a[j] = tmp;

                // Insertion a[i] between a[i-h], a[i-2*h], a[i-3h] ... .
//                for (int j = i; j >= h && less(a[j], a[j-h]); j -= h)
//                    exch(a, j, j-h);

                if (draw) draw("Shell", a, h);
            }
            h /= 3;
        }
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int[] tmp = in.readAllInts();
        Integer[] a = new Integer[tmp.length];
        for (int i = 0; i < tmp.length; i++)
            a[i] = tmp[i];

        Shell.sort(a, DRAW);
        assert Shell.isSorted(a) : "Error in sorting";
        Shell.show(a);
    }
}
