public class MyInt implements Comparable<MyInt> {
    private final int i;

    public MyInt(int i) { this.i = i; }

    public int get() { return this.i; }

    @Override
    public int compareTo(MyInt that) {
        if (this.i < that.i) return -1;
        else if (this.i > that.i) return 1;
        return 0;
    }

    @Override
    public String toString() { return Integer.toString(this.i); }
}
