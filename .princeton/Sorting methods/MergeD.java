public class MergeD extends Sorting {
    private static Comparable[] aux;
    private static int CUTOFF = 7;

    // merge two arrays a[lo...mid] & a[mid+1...hi]
    public static void merge(Comparable[] a, int lo, int mid, int hi) {
        assert isSorted(a, lo, mid) : "Error in sorting part left";
        assert isSorted(a, mid+1, hi) : "Error in sorting part right";
        int i = lo, j = mid+1;
        // copy a[lo...hi] to aux[lo...hi]
        System.arraycopy(a, lo, aux, lo, hi-lo+1);
        // merge back with compare
        for (int k = lo; k <= hi; k++) {
            if      (i > mid)               a[k] = aux[j++];
            else if (j > hi)                a[k] = aux[i++];
            else if (less(aux[i], aux[j]))  a[k] = aux[i++];
            else                            a[k] = aux[j++];
        }
        assert isSorted(a, lo, hi) : "Error in merge";
    }

    // main recursion call
    public static void sort(Comparable[] a) { sort(a, NO_DRAW); }
    public static void sort(Comparable[] a, boolean draw) {
        aux = new Comparable[a.length];
        sort(a, draw, 0, a.length-1);
    }

    // sub-recursion call -> sorting a[lo...hi]
    public static void sort(Comparable[] a, boolean draw, int lo, int hi) {
//        if (hi <= lo) return;
        if (hi <= lo + CUTOFF) {
            subSort(a, draw, lo, hi);
            return;
        }
        int mid = lo + (hi - lo) / 2;
        // sort left half of array
        sort(a, draw, lo, mid);
        // sort left right of array
        sort(a, draw, mid+1, hi);
        // merge resulting sub-array
        merge(a, lo, mid, hi);
        if (draw) draw("Merge down", a, hi);
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int[] tmp = in.readAllInts();
        Integer[] a = new Integer[tmp.length];
        for (int i = 0; i < tmp.length; i++)
            a[i] = tmp[i];

        MergeD.sort(a, DRAW);
        assert MergeD.isSorted(a) : "Error in sorting";
        MergeD.show(a);
    }
}

