import java.util.Arrays;

public class Fast {
    public static void main(String[] args) {
    /* YOUR CODE HERE */
        // rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);
        StdDraw.setPenRadius(0.002);  // make the points a bit larger

        // read in the input
        String filename = args[0];
        In in = new In(filename);
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }

        for (int i = 0; i < N-3; i++) {
            Arrays.sort(points);
            Point p1 = points[i];
            Arrays.sort(points, p1.SLOPE_ORDER);
            int beg, end;
            for (beg = 1; beg < N-2; beg = ++end) {
                end = beg;
                double slope = p1.slopeTo(points[beg]);
                while (p1.slopeTo(points[end+1]) == slope)
                        if (++end >= N-1) break;

                if (end - beg > 1 && p1.compareTo(points[beg]) < 0) {
                    String vector = p1 + " -> ";
                    for (int j = beg; j < end; j++)
                        vector += points[j] + " -> ";
                    vector += points[end];
                    StdOut.println(vector);
                    p1.drawTo(points[end]);
                }
            }
        }

        // display to screen all at once
        StdDraw.show(0);

        // reset the pen radius
        StdDraw.setPenRadius();
    }
}

