import java.util.Arrays;

public class Brute {
    public static void main(String[] args) {
        /* YOUR CODE HERE */
        // rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);
        StdDraw.setPenRadius(0.002);  // make the points a bit larger

        // read in the input
        String filename = args[0];
        In in = new In(filename);
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }

        Arrays.sort(points);
        for (int i = 0; i < N-3; i++) {
            for (int j = i; j < N-2; j++) {
                double slope1 = points[i].slopeTo(points[j]);
                for (int k = j+1; k < N-1; k++) {
                    double slope2 = points[i].slopeTo(points[k]);
                    if (slope1 != slope2) continue;
                    for (int l = k+1; l < N; l++) {
                        double slope3 = points[i].slopeTo(points[l]);
                        if (slope1 == slope3) {
                            StdOut.println(points[i] + " -> " + points[j]
                                    + " -> " + points[k] + " -> " + points[l]);
                            points[i].drawTo(points[l]);
                        }
                    }
                }
            }
        }

        // display to screen all at once
        StdDraw.show(0);

        // reset the pen radius
        StdDraw.setPenRadius();
    }
}

