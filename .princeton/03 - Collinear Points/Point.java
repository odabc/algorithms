/*************************************************************************
 * Name:
 * Email:
 *
 * Compilation:  javac Point.java
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for points in the plane.
 *
 *************************************************************************/

import java.util.Arrays;
import java.util.Comparator;

public class Point implements Comparable<Point> {

    // compare points by slope
    public final Comparator<Point> SLOPE_ORDER = new BySlopeOrder();
    // YOUR DEFINITION HERE
    private class BySlopeOrder implements Comparator<Point> {
        public int compare(Point p1, Point p2) {
            if ((p1 == null) || (p2 == null))
                throw new NullPointerException("some points not defined");
            double s1 = slopeTo(p1);
            double s2 = slopeTo(p2);
            return Double.compare(s1, s2);
        }
    }

    private final int x;                              // x coordinate
    private final int y;                              // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    // slope between this point and that point
    public double slopeTo(Point that) {
        /* YOUR CODE HERE */
        if (that == null)
            throw new NullPointerException("slope point not defined");
        if (this.y == that.y)
            if (this.x == that.x) return Double.NEGATIVE_INFINITY;
            else return 0.0;
        else if (this.x == that.x) return Double.POSITIVE_INFINITY;
        return ((double) that.y - this.y) / (that.x - this.x);
    }

    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    @Override
    public int compareTo(Point that) {
        if (that == null)
            throw new NullPointerException("compare point not defined");
        /* YOUR CODE HERE */
        if (this.y < that.y) return -1;
        else if (this.y > that.y) return 1;
        else if (this.x < that.x) return -1;
        else if (this.x > that.x) return 1;
        else return 0;
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    // unit test
    public static void main(String[] args) {
        /* YOUR CODE HERE */
        // read in the input
        String filename = args[0];
        In in = new In(filename);
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }
        Arrays.sort(points);
        for (Point point : points)
            StdOut.println(point);

        for (int i = 0; i < N-3; i++) {
            for (int j = i; j < N-2; j++) {
                double slope1 = points[i].slopeTo(points[j]);
                for (int k = j+1; k < N-1; k++) {
                    double slope2 = points[i].slopeTo(points[k]);
                    if (slope1 != slope2) continue;
                    for (int l = k+1; l < N; l++) {
                        double slope3 = points[i].slopeTo(points[l]);
                        if (slope1 == slope3) {
                            StdOut.println(points[i] + " -> " + points[j]
                                    + " -> " + points[k] + " -> " + points[l]);
                            points[i].drawTo(points[l]);
                        }
                    }
                }
            }
        }

    }
}
