import java.util.NoSuchElementException;

public class Subset {
    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        if (k < 0)
            throw new IllegalArgumentException("The number must be >= 0.");

        RandomizedQueue<String> strings = new RandomizedQueue<String>();
        int s = 0;
        while (!StdIn.isEmpty()) {
            if (k > s++) strings.enqueue(StdIn.readString());
            else if (StdRandom.uniform(s) < k) {
                strings.dequeue();
                strings.enqueue(StdIn.readString());
            } else StdIn.readString();
        }
        if (s < k)
            throw new NoSuchElementException("Too few elements.");

        for (String string : strings)
            System.out.println(string);
    }
}