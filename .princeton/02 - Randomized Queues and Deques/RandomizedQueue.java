import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] var;
    private int N;

    // construct an empty randomized queue
    public RandomizedQueue() { var = (Item[]) new Object[2]; }

    // is the queue empty?
    public boolean isEmpty() { return N == 0; }

    // return the number of items on the queue
    public int size() { return N; }

    // resize the underlying array holding the elements
    private void resize(int capacity) {
        assert capacity >= N;
        var = Arrays.copyOf(var, capacity);
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) throw new NullPointerException("wrong item value");
        // array.length < 16 multiply by 2, else by 1.5
        if (var.length == N) {
            if ((var.length >> 4) > 0) resize(var.length + (var.length >> 1));
            else resize(var.length << 1);
        }
        var[N++] = item;
    }

    // delete and return a random item
    // StdRandom.uniform(N) - returns an integer
    // uniformly between 0 (inclusive) and N (exclusive).
    public Item dequeue() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        int pos = StdRandom.uniform(N);
        Item item = var[pos];
        var[pos] = var[--N];
        var[N] = null;

        // shrink size of array if necessary
        if (N > 0 && N == var.length/4) resize(var.length >> 1);
        return item;
    }

    // return (but do not delete) a random item
    // StdRandom.uniform(N) - returns an integer
    // uniformly between 0 (inclusive) and N (exclusive).
    public Item sample() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        return var[StdRandom.uniform(N)];
    }

//    @Override
//    public String toString() {
//        StringBuilder s = new StringBuilder();
//        for (Item item : this)
//            s.append(item + " ");
//        return s.toString();
//    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private int[] queue;
        private int pos;

        public RandomizedQueueIterator() {
            if (!isEmpty()) {
                queue = new int[N];
                for (int i = 0; i < N; i++)
                    queue[i] = i;
                StdRandom.shuffle(queue);
                pos = N;
            }
        }

        @Override
        public boolean hasNext() { return pos != 0; }

        @Override
        public Item next() {
            if (pos <= 0) throw new NoSuchElementException("Queue underflow");
            return var[queue[--pos]];
        }

        @Override
        public void remove() { throw new UnsupportedOperationException(); }

    }

    // unit testing
    public static void main(String[] args) {
        RandomizedQueue<String> strings = new RandomizedQueue<String>();
        while (!StdIn.isEmpty()) {
        /* Input stream format: a <string> -> enqueue, s -> sample, d -> dequeue */
            String oper = StdIn.readString();
            if (oper.equals("a")) strings.enqueue(StdIn.readString());
            else if (oper.equals("s"))
                System.out.println(strings.sample() + " selected.");
            else if (oper.equals("d"))
                System.out.println(strings.dequeue() + " deleted.");
            else System.out.println("Error coding input string sequence.");

            // foreach testing
//            System.out.print(strings);
            for (String st : strings)
                System.out.print(st + " ");
            System.out.println("(" + strings.size() + " left in Queue)");
        }

    }
}
