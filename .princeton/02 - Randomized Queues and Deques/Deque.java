import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
    private Node first;
    private Node last;
    private int N;

    private class Node {
        private Item item;
        private Node next;
        private Node prev;
    }

    // construct an empty deque
    public Deque() {
        first = null;
        last = null;
        N = 0;
    }
    // is the deque empty?
    public boolean isEmpty() { return first == null; }

    // return the number of items on the deque
    public int size() { return N; }

    // insert the item at the front
    public void addFirst(Item item) {
        if (item == null) throw new NullPointerException("wrong item value");
        if (!isEmpty()) {
            Node oldFirst = first;
            first = new Node();
            first.item = item;
            first.next = oldFirst;
            oldFirst.prev = first;
        } else {
            first = new Node();
            first.item = item;
            first.next = null;
            first.prev = null;
            last = first;
        }
        N++;
    }

    // insert the item at the end
    public void addLast(Item item) {
        if (item == null) throw new NullPointerException("wrong item value");
        if (!isEmpty()) {
            Node oldLast = last;
            last = new Node();
            last.item = item;
            last.prev = oldLast;
            oldLast.next = last;
        } else {
            last = new Node();
            last.item = item;
            last.next = null;
            last.prev = null;
            first = last;
        }
        N++;
    }

    // delete and return the item at the front
    public Item removeFirst() {
        if (isEmpty()) throw new NoSuchElementException("Deque underflow");
        Item item = first.item;
        if (first.next != null) {
            first = first.next;
            first.prev = null;
            N--;
        } else {
            first = null;
            last = null;
            N = 0;
        }
        return item;
    }

    // delete and return the item at the end
    public Item removeLast() {
        if (isEmpty()) throw new NoSuchElementException("Deque underflow");
        Item item = last.item;
        if (last.prev != null) {
            last = last.prev;
            last.next = null;
            N--;
        } else {
            first = null;
            last = null;
            N = 0;
        }
        return item;
    }

//    @Override
//    public String toString() {
//        StringBuilder s = new StringBuilder();
//        for (Item item : this)
//            s.append(item + " ");
//        return s.toString();
//    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {  return new DequeIterator();     }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        @Override
        public boolean hasNext()    { return current != null; }

        @Override
        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next;
            return item;
        }

        @Override
        public void remove() { throw new UnsupportedOperationException(); }
    }

    // unit testing
    public static void main(String[] args) {
        Deque<String> strings = new Deque<String>();
        while (!StdIn.isEmpty()) {
            /* Input stream format:
            +f <string> -> addFirst,     +l <string> -> addLast
            -f <string> -> removeFirst,  -l <string> -> removeLast */
            String oper = StdIn.readString();
            if (oper.equals("+f")) strings.addFirst(StdIn.readString());
            else if (oper.equals("+l")) strings.addLast(StdIn.readString());
            else if (oper.equals("-f"))
                System.out.println(strings.removeFirst() + " deleted.");
            else if (oper.equals("-l"))
                System.out.println(strings.removeLast()  + " deleted.");
            else System.out.println("Error coding input string sequence.");

            // foreach testing
//            System.out.print(strings);
            for (String st : strings)
                System.out.print(st + " ");
            System.out.println("(" + strings.size() + " left in Deque)");
        }
    }
}