public class PercolationStats {
    private double[] results;

    // perform T independent experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0)
            throw new IllegalArgumentException("grid size should be > 0");
        if (T <= 0)
            throw new
                    IllegalArgumentException("number of experiments should be > 0");
        results = new double[T];
        // perform T independent experiments on an N-by-N grid
        for (int t = 0; t < T; t++) {
            Percolation percolation = new Percolation(N);
            int count = 0;
            while (true) {
                int i = StdRandom.uniform(N) + 1;
                int j = StdRandom.uniform(N) + 1;
                if (!percolation.isOpen(i, j)) {
                    percolation.open(i, j);
                    count++;
                }
                if (percolation.percolates()) {
                    results[t] = (double) count / (N * N);
                    break;
                }
            }
        }
    }

    // sample mean of percolation threshold
    public double mean() { return StdStats.mean(results); }

    // sample standard deviation of percolation threshold
    public double stddev() { return StdStats.stddev(results); }

    // low  endpoint of 95% confidence interval
    public double confidenceLo() {
        return mean() - 1.96 * stddev() / Math.sqrt(results.length); }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return mean() + 1.96 * stddev() / Math.sqrt(results.length); }

    // test client (described below)
    public static void main(String[] args) {
        int N = StdIn.readInt();
        int T = StdIn.readInt();
        Stopwatch time = new Stopwatch();
        PercolationStats test = new PercolationStats(N, T);

        StdOut.println("mean                    = " + test.mean());
        StdOut.println("stddev                  = " + test.stddev());
        StdOut.println("95% confidence interval = "
                + test.confidenceLo() + ", " + test.confidenceHi());

        StdOut.println(time.elapsedTime() * 1000);

    }
}