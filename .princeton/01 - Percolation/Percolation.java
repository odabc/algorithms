import java.util.BitSet;

public class Percolation {
    private WeightedQuickUnionUF ways;
    private BitSet sites;
    private BitSet hi;
    private BitSet lo;
    private int max;
    private boolean percolates;

    // create N-by-N grid, with all sites blocked
    public Percolation(int N) {
        if (N <= 0)
            throw new IllegalArgumentException("grid size should be > 0");
        ways = new WeightedQuickUnionUF(N * N);
        sites = new BitSet(N * N);
        hi = new BitSet(N * N);
        lo = new BitSet(N * N);
        max = N;
        percolates = false;
        for (int i = 1; i <= max; i++) {
            hi.set(rcTo1D(1, i));
            lo.set(rcTo1D(max, i));
        }
    }

    // open site (row i, column j) if it is not open already
    public void open(int i, int j) {
        if (isOpen(i, j)) return; // already opened

        sites.set(rcTo1D(i, j));
        int root;
        boolean newHi = hi.get(rcTo1D(i, j));
        boolean newLo = lo.get(rcTo1D(i, j));

        // check and connect to neighbouring sites
        if ((i > 1) && sites.get(rcTo1D(i - 1, j))) {
            root = ways.find(rcTo1D(i - 1, j));
            newHi |= hi.get(root);
            newLo |= lo.get(root);
            ways.union(rcTo1D(i, j), rcTo1D(i - 1, j));
        }
        if ((i < max) && sites.get(rcTo1D(i + 1, j))) {
            root = ways.find(rcTo1D(i + 1, j));
            newHi |= hi.get(root);
            newLo |= lo.get(root);
            ways.union(rcTo1D(i, j), rcTo1D(i + 1, j));
        }
        if ((j > 1) && sites.get(rcTo1D(i, j - 1))) {
            root = ways.find(rcTo1D(i, j - 1));
            newHi |= hi.get(root);
            newLo |= lo.get(root);
            ways.union(rcTo1D(i, j), rcTo1D(i, j - 1));
        }
        if ((j < max) && sites.get(rcTo1D(i, j + 1))) {
            root = ways.find(rcTo1D(i, j + 1));
            newHi |= hi.get(root);
            newLo |= lo.get(root);
            ways.union(rcTo1D(i, j), rcTo1D(i, j + 1));
        }

        root = ways.find(rcTo1D(i, j));
        if (newHi) hi.set(root);
        if (newLo) lo.set(root);

        percolates |= newHi && newLo;
    }

    // is site (row i, column j) open?
    public boolean isOpen(int i, int j) { return sites.get(rcTo1D(i, j)); }

    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        return isOpen(i, j) && hi.get(ways.find(rcTo1D(i, j))); }

    // does the system percolate?
    public boolean percolates() { return percolates; }

    // convert 2D array to 1D array with start from zero index
    private int rcTo1D(int i, int j) {
        if ((i < 1) || (i > max))
            throw new IndexOutOfBoundsException("row index out of bounds");
        if ((j < 1) || (j > max))
            throw new IndexOutOfBoundsException("column index out of bounds");
        return (i - 1) * max + j - 1;
    }

    // test client (optional)
    public static void main(String[] args) {
        int N = StdIn.readInt();
        Percolation percolation = new Percolation(N);
        while (!StdIn.isEmpty()) {
            int i = StdIn.readInt();
            int j = StdIn.readInt();
            if (percolation.isOpen(i, j)) continue;
            percolation.open(i, j);

            if (percolation.percolates()) StdOut.println("Percolation");
            else StdOut.println("Isolated");
        }

    }
}