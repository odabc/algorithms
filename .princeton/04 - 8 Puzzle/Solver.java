public class Solver {
    private final Stack<Board> solution;
    private int moves;
    private boolean isSolvable;

    private class Node implements Comparable<Node> {
        private Board board;
        private int priority;
        private int moves;
        private Node previous;

        Node(Board board, int moves, Node previous) {
            this.board = board;
            this.moves = moves;
            this.priority = moves + board.manhattan();
            this.previous = previous;
        }

        private int priority() { return priority; }

        @Override
        public int compareTo(Node that) {
            if (this.priority() < that.priority()) {
                return -1;
            }
            if (this.priority() > that.priority()) {
                return +1;
            }
            return 0;
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        solution = new Stack<Board>();
        moves = 0;
        isSolvable = false;

        int N = initial.dimension();
        int[][] sol = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                    sol[i][j] = i*N + j + 1;
        sol[N-1][N-1] = 0;

        Board goal = new Board(sol);

        if (initial.equals(goal)) {
            isSolvable = true;
            solution.push(initial);
            return;
        } else if (initial.twin().equals(goal)) {
            isSolvable = false;
            return;
        }

        // A* search algorithm
        MinPQ<Node> minPQ = new MinPQ<Node>();
        minPQ.insert(new Node(initial, 0, null));
        Node node = checkPQ(minPQ, goal);

        while (node == null)
            node = checkPQ(minPQ, goal);

        // check return good results or for twin board
        if (node.board.equals(goal)) {
            isSolvable = true;
            moves = node.moves;
            this.solution.push(node.board);
            while (node.previous != null) {
                node = node.previous;
                this.solution.push(node.board);
            }
        } else {
            isSolvable = false;
            moves = -1;
            while (!solution.isEmpty())
                solution.pop();
        }
    }

    private Node checkPQ(MinPQ<Node> minPQ, Board goal) {
        Node node = minPQ.delMin();

        Board board = node.board;
        // for 1-priority queue solution check twin here
        if (board.equals(goal) || board.twin().equals(goal)) return node;

        // add next moves for find solution
        node.moves++;
        Iterable<Board> neighbors = node.board.neighbors();
        for (Board neighbor : neighbors) {
            // check cycling moves prev -> now -> next, where prev == next
            if (node.previous != null
                    && neighbor.equals(node.previous.board))
                continue;
            Node newNode = new Node(neighbor, node.moves, node);
            minPQ.insert(newNode);
        }
        return null;
    }

    // is the initial board solvable?
    public boolean isSolvable() { return isSolvable; }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() { return moves; }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (isSolvable())
            return solution;
        return null;
    }

    // solve a slider puzzle (given below)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        Stopwatch timer = new Stopwatch();

        // solve the puzzle
        Solver solver = new Solver(initial);

        StdOut.println(timer.elapsedTime());

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}