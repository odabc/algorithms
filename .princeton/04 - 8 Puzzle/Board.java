public class Board {
    private final short[] tiles;
    private final int N;

    // construct a board from an N-by-N array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        N = blocks.length;
        tiles = new short[N*N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                tiles[p(i, j)] = (short) blocks[i][j];
    }

    // 2D-index to 1D-index
    private int p(int i, int j)  { return  i * N + j; }
    // 1D-index to 2D-row index
    private int i(int p)  { return p / N; }
    // 1D-index to 2D-col index
    private int j(int p)  { return p % N; }

    // board dimension N
    public int dimension() { return this.N; }

    // number of blocks out of place
    public int hamming() {
        int ret = 0;
        int max = tiles.length;
        for (int p = 0; p < max; p++) {
            short v = tiles[p];
            if (v != p+1 && v > 0)
                ret++;
        }
        return ret;
    }

    // sum of Manhattan distances between blocks and goal
    public int manhattan() {
        int ret = 0;
        int max = tiles.length;
        for (int p = 0; p < max; p++) {
            short v = tiles[p];
            short d = (short) (Math.abs(i(p)-i(v-1)) + Math.abs(j(p)-j(v-1)));
            if (d > 0 && v > 0)
                ret += d;
        }
        return ret;
    }

    // is this board the goal board?
    public boolean isGoal() { return hamming() == 0; }

    // a board that is obtained by exchanging two adjacent blocks in the same row
    public Board twin() {
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = tiles[p(i, j)];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N - 1; j++) {
                if (blocks[i][j] > 0 && blocks[i][j + 1] > 0) {
                    int t = blocks[i][j];
                    blocks[i][j] = blocks[i][j+1];
                    blocks[i][j+1] = t;
                    return new Board(blocks);
                }
            }
        }
        return null;
    }

    // does this board equal y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Board that = (Board) y;
        int max = tiles.length;
        for (int p = 0; p < max; p++)
                if (tiles[p] != that.tiles[p]) return false;
        return true;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        Stack<Board> boards = new Stack<Board>();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = tiles[p(i, j)];

        int p0 = 0;
        int max = tiles.length;
        while (tiles[p0] > 0 && p0 < max) p0++;
        int i0 = i(p0);
        int j0 = j(p0);

        Board[] tmp = new Board[4];
        if (i0 > 0) {
            tmp[0] = new Board(blocks);
            tmp[0].tiles[p0] = tmp[0].tiles[p0-N];
            tmp[0].tiles[p0-N] = 0;
        }
        if (i0 < N-1) {
            tmp[1] = new Board(blocks);
            tmp[1].tiles[p0] = tmp[1].tiles[p0+N];
            tmp[1].tiles[p0+N] = 0;
        }
        if (j0 > 0) {
            tmp[2] = new Board(blocks);
            tmp[2].tiles[p0] = tmp[2].tiles[p0-1];
            tmp[2].tiles[p0-1] = 0;
        }
        if (j0 < N-1) {
            tmp[3] = new Board(blocks);
            tmp[3].tiles[p0] = tmp[3].tiles[p0+1];
            tmp[3].tiles[p0+1] = 0;
        }

        // some improvements: sorting results
        // this is more increase speed future calculations
        for (int i = 0; i < 4; i++) {
            if (tmp[i] == null) continue;
            for (int j = i+1; j < 4; j++) {
                if (tmp[j] == null) continue;

                if (tmp[i].hamming() > tmp[j].hamming()) {
                    Board board = tmp[i];
                    tmp[i] = tmp[j];
                    tmp[j] = board;
                }
            }
        }

        for (Board board : tmp)
            if (board != null) boards.push(board);
        return boards;
    }

    // string representation of this board (in the output format specified below)
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(N + "\n");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                s.append(String.format("%2d ", tiles[p(i, j)]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    // unit tests (not graded)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board board = new Board(blocks);

        StdOut.println(board);
        StdOut.println("Dimension: = " + board.dimension());
        StdOut.println("Hamming:   = " + board.hamming());
        StdOut.println("Manhattan: = " + board.manhattan());
        StdOut.println("Twin board:");
        StdOut.println(board.twin());

        StdOut.println("Variation next step boards:");
        Iterable<Board> boards = board.neighbors();
        for (Board var : boards)
            StdOut.println(var);
    }
}